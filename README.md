<div align="center">
  <h1>Ask Me Anything</h1>
</div>

<p align="center">
  <kbd><a href="https://gitlab.com/chadporter/ama/issues/new">Ask a question</a></kbd> <kbd><a href="https://gitlab.com/chadporter/ama/issues?scope=all&state=closed">Read questions</a></kbd>
</p>

<br />

Anything means *anything*. Personal questions. Work. Life. Code. Whatever.

## Guidelines

- :mag: Ensure your question hasn't already been answered.
- :memo: Use a succinct title and description.
- :bug: Bugs & feature requests should be opened on the relevant issue tracker.
- :signal_strength: Support questions are better asked on Stack Overflow.
- :blush: Be nice, civil and polite.
- :heart_eyes: If you include at least one emoji in your question, the feedback will probably come faster.


## Links

- [Read more AMAs](https://github.com/sindresorhus/amas)
- [What's an AMA?](https://en.wikipedia.org/wiki/Reddit#IAmA_and_AMA)